package sheridan;

import static org.junit.Assert.*;


import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Result does not match expected", Palindrome.isPalindrome( "anna" ));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Result does not match expected", Palindrome.isPalindrome( "apple" ));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Result does not match expected", Palindrome.isPalindrome( "edit tide" ));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Result does not match expected", Palindrome.isPalindrome( "edit on tide" ));
	}	
	
}
